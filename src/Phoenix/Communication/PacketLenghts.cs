using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Communication
{
    static class Packetlenght
    {
        /// <summary>
        /// Dynamic packet means that packet size follows id byte and it is sent as ushort.
        /// </summary>
        public const int Dynamic = 0x8000;
        private static int[] length;

        /// <summary>
        /// Gets packet lenght.
        /// </summary>
        /// <param name="id">Packet id.</param>
        /// <returns>Returns packet lenght or Dynamic. If lenght is zero packet lenght is unknown or stream is corrupted.</returns>
        public static int GetLenght(byte id)
        {
            return length[id];
        }

        public static void SetNewLength(byte id, int value)
        {
            length[id] = value;
        }

        static Packetlenght()
        {
            length = new int[256];

            length[0x00] = 0x0068; // Character Creation
            length[0x01] = 0x0005; // Logout
            length[0x02] = 0x0007; // Request Movement
            length[0x03] = Dynamic; // Speech
            length[0x04] = 0x0002; // Request God Mode
            length[0x05] = 0x0005; // Attack
            length[0x06] = 0x0005; // Double Click
            length[0x07] = 0x0007; // Take Object
            length[0x08] = 0x000E; // Drop Object
            length[0x09] = 0x0005; // Single Click
            length[0x0A] = 0x000B; // Edit
            length[0x0B] = 0x000A; // Edit Area
            length[0x0C] = Dynamic; // Tile Data
            length[0x0D] = 0x0003; // NPC Data
            length[0x0E] = Dynamic; // Edit Template Data
            length[0x0F] = 0x003D; // Paperdoll Old
            length[0x10] = 0x00D7; // Hue Data
            length[0x11] = Dynamic; // Mobile Stats
            length[0x12] = Dynamic; // God Command
            length[0x13] = 0x000A; // Equip Item Request
            length[0x14] = 0x0006; // Change Elevation
            length[0x15] = 0x0009; // Follow
            length[0x16] = 0x0001; // Request Script Names
            length[0x17] = Dynamic; // Script Tree Command
            length[0x18] = Dynamic; // Script Attach
            length[0x19] = Dynamic; // NPC Conversation Data
            length[0x1A] = Dynamic; // Show Item
            length[0x1B] = 0x0025; // Login Confirm
            length[0x1C] = Dynamic; // Text
            length[0x1D] = 0x0005; // Destroy
            length[0x1E] = 0x0004; // Animate
            length[0x1F] = 0x0008; // Explode
            length[0x20] = 0x0013; // Teleport
            length[0x21] = 0x0008; // Block Movement
            length[0x22] = 0x0003; // Accept Movement/Resync Request
            length[0x23] = 0x001A; // Drag Item
            length[0x24] = 0x0007; // Open Container
            length[0x25] = 0x0014; // Object to Object
            length[0x26] = 0x0005; // Old Client
            length[0x27] = 0x0002; // Get Item Failed
            length[0x28] = 0x0005; // Drop Item Failed
            length[0x29] = 0x0001; // Drop Item OK
            length[0x2A] = 0x0005; // Blood
            length[0x2B] = 0x0002; // God Mode
            length[0x2C] = 0x0002; // Death
            length[0x2D] = 0x0011; // Health
            length[0x2E] = 0x000F; // Equip Item
            length[0x2F] = 0x000A; // Swing
            length[0x30] = 0x0005; // Attack OK
            length[0x31] = 0x0001; // Attack End
            length[0x32] = 0x0002; // Hack Mover
            length[0x33] = 0x0002; // Group
            length[0x34] = 0x000A; // Client Query
            length[0x35] = 0x008D; // Resource Type
            length[0x36] = Dynamic; // Resource Tile Data
            length[0x37] = 0x0008; // Move Object
            length[0x38] = 0x0007; // Follow Move
            length[0x39] = 0x0009; // Groups
            length[0x3A] = Dynamic; // Skills
            length[0x3B] = Dynamic; // Accept Offer
            length[0x3C] = Dynamic; // Container Contents
            length[0x3D] = 0x0002; // Ship
            length[0x3E] = 0x0025; // Versions
            length[0x3F] = Dynamic; // Update Statics
            length[0x40] = 0x00C9; // Update Terrain
            length[0x41] = Dynamic; // Update Tiledata
            length[0x42] = Dynamic; // Update Art
            length[0x43] = 0x0029; // Update Anim
            length[0x44] = 0x00C9; // Update Hues
            length[0x45] = 0x0005; // Ver OK
            length[0x46] = Dynamic; // New Art
            length[0x47] = 0x000B; // New Terrain
            length[0x48] = 0x0049; // New Anim
            length[0x49] = 0x005D; // New Hues
            length[0x4A] = 0x0005; // Destroy Art
            length[0x4B] = 0x0009; // Check Ver
            length[0x4C] = Dynamic; // Script Names
            length[0x4D] = Dynamic; // Script File
            length[0x4E] = 0x0006; // Light Change
            length[0x4F] = 0x0002; // Sunlight
            length[0x50] = Dynamic; // Board Header
            length[0x51] = Dynamic; // Board Message
            length[0x52] = Dynamic; // Post Message
            length[0x53] = 0x0002; // Login Reject
            length[0x54] = 0x000C; // Sound
            length[0x55] = 0x0001; // Login Complete
            length[0x56] = 0x000B; // Map Command
            length[0x57] = 0x006E; // Update Regions
            length[0x58] = 0x006A; // New Region
            length[0x59] = Dynamic; // New Context FX
            length[0x5A] = Dynamic; // Update Context FX
            length[0x5B] = 0x0004; // Game Time
            length[0x5C] = 0x0002; // Restart Ver
            length[0x5D] = 0x0049; // Pre Login
            length[0x5E] = Dynamic; // Server List
            length[0x5F] = 0x0031; // Add Server
            length[0x60] = 0x0005; // Server Remove
            length[0x61] = 0x0009; // Destroy Static
            length[0x62] = 0x000F; // Move Static
            length[0x63] = 0x000D; // Area Load
            length[0x64] = 0x0001; // Area Load Request
            length[0x65] = 0x0004; // Weather Change
            length[0x66] = Dynamic; // Book Contents
            length[0x67] = 0x0015; // Simple Edit
            length[0x68] = Dynamic; // Script LS Attach
            length[0x69] = Dynamic; // Friends
            length[0x6A] = 0x0003; // Friend Notify
            length[0x6B] = 0x0009; // Key Use
            length[0x6C] = 0x0013; // Target
            length[0x6D] = 0x0003; // Music
            length[0x6E] = 0x000E; // Animation
            length[0x6F] = Dynamic; // Trade
            length[0x70] = 0x001C; // Effect
            length[0x71] = Dynamic; // Bulletin Board
            length[0x72] = 0x0005; // Combat
            length[0x73] = 0x0002; // Ping
            length[0x74] = Dynamic; // Shop Data
            length[0x75] = 0x0023; // Rename MOB
            length[0x76] = 0x0010; // Server Change
            length[0x77] = 0x0011; // Naked MOB
            length[0x78] = Dynamic; // Equipped MOB
            length[0x79] = 0x0009; // Resource Query
            length[0x7A] = Dynamic; // Resource Data
            length[0x7B] = 0x0002; // Sequence
            length[0x7C] = Dynamic; // Object Picker
            length[0x7D] = 0x000D; // Picked Object
            length[0x7E] = 0x0002; // God View Query
            length[0x7F] = Dynamic; // God View Data
            length[0x80] = 0x003E; // Account Login Request
            length[0x81] = Dynamic; // Account Login OK
            length[0x82] = 0x0002; // Account Login Failed
            length[0x83] = 0x0027; // Account Delete Character
            length[0x84] = 0x0045; // Change Character Password
            length[0x85] = 0x0002; // Delete Character Failed
            length[0x86] = Dynamic; // All Characters
            length[0x87] = Dynamic; // Send Resources
            length[0x88] = 0x0042; // Open Paperdoll
            length[0x89] = Dynamic; // Corpse Equipment
            length[0x8A] = Dynamic; // Trigger Edit
            length[0x8B] = Dynamic; // Display Sign
            length[0x8C] = 0x000B; // Server Redirect
            length[0x8D] = Dynamic; // Unused3
            length[0x8E] = Dynamic; // Move Character
            length[0x8F] = Dynamic; // Unused4
            length[0x90] = 0x0013; // Open Course Gump
            length[0x91] = 0x0041; // Post Login
            length[0x92] = Dynamic; // Update Multi
            length[0x93] = 0x0063; // Book Header
            length[0x94] = Dynamic; // Update Skill
            length[0x95] = 0x0009; // Hue Picker
            length[0x96] = Dynamic; // Game Central Monitor
            length[0x97] = 0x0002; // Move Player
            length[0x98] = Dynamic; // MOB Name
            length[0x99] = 0x001A; // Target Multi
            length[0x9A] = Dynamic; // Text Entry
            length[0x9B] = 0x0102; // Request Assistance
            length[0x9C] = 0x0035; // Assist Request
            length[0x9D] = 0x0033; // GM Single
            length[0x9E] = Dynamic; // Shop Sell
            length[0x9F] = Dynamic; // Shop Offer
            length[0xA0] = 0x0003; // Server Select
            length[0xA1] = 0x0009; // HP Health
            length[0xA2] = 0x0009; // Mana Health
            length[0xA3] = 0x0009; // Fat Health
            length[0xA4] = 0x0095; // Hardware Info
            length[0xA5] = Dynamic; // Web Browser
            length[0xA6] = Dynamic; // Message
            length[0xA7] = 0x0004; // Request Tip
            length[0xA8] = Dynamic; // Server List
            length[0xA9] = Dynamic; // Character List
            length[0xAA] = 0x0005; // Current Target
            length[0xAB] = Dynamic; // String Query
            length[0xAC] = Dynamic; // String Response
            length[0xAD] = Dynamic; // Speech Unicode
            length[0xAE] = Dynamic; // Text Unicode
            length[0xAF] = 0x000D; // Death Animation
            length[0xB0] = Dynamic; // Generic Gump
            length[0xB1] = Dynamic; // Generic Gump Trigger
            length[0xB2] = Dynamic; // Chat Message
            length[0xB3] = Dynamic; // Chat Text
            length[0xB4] = Dynamic; // Target Object List
            length[0xB5] = 0x0040; // Open Chat
            length[0xB6] = 0x0009; // Help Request
            length[0xB7] = Dynamic; // Help Text
            length[0xB8] = Dynamic; // Character Profile
            length[0xB9] = 0x0003; // Features
            length[0xBA] = 0x0006; // Pointer
            length[0xBB] = 0x0009; // Account ID
            length[0xBC] = 0x0003; // Game Season
            length[0xBD] = Dynamic; // Client Version
            length[0xBE] = Dynamic; // Assist Version
            length[0xBF] = Dynamic; // Generic Command
            length[0xC0] = 0x0024; // Hued FX
            length[0xC1] = Dynamic; // Localized Text
            length[0xC2] = Dynamic; // Unicode Text Entry
            length[0xC3] = Dynamic; // Global Queue
            length[0xC4] = 0x0006; // Semivisible
            length[0xC5] = 0x00CB; // Invalid Map
            length[0xC6] = 0x0001; // Invalid Map Enable
            length[0xC7] = 0x0031; // Particle Effect
            length[0xC8] = 0x0002; // Change Update Range
            length[0xC9] = 0x0006; // Trip Time
            length[0xCA] = 0x0006; // UTrip Time
            length[0xCB] = 0x0007; // Global Queue Count
            length[0xCC] = Dynamic; // Localized Text Plus String
            length[0xCD] = 0x0001; // Unknown God Packet
            length[0xCE] = Dynamic; // IGR Client
            length[0xCF] = 0x004E; // IGR Login
            length[0xD0] = Dynamic; // IGR Configuration
            length[0xD1] = 0x0002; // IGR Logout
            length[0xD2] = 0x0019; // Update Mobile
            length[0xD3] = Dynamic; // Show Mobile
            length[0xD4] = Dynamic; // Book Info
            length[0xD5] = Dynamic; // Unknown Client Packet
            length[0xD6] = Dynamic; // Mega Cliloc
            length[0xD7] = Dynamic; // AOS Command
            length[0xD8] = Dynamic; // Custom House
            length[0xD9] = 0x010C; // Metrics
            length[0xDA] = Dynamic; // Mahjong
            length[0xDB] = Dynamic; // Character Transfer Log
            length[0xDC] = 0x0009; // Equipment Description
            length[0xDD] = Dynamic; // Compressed Gump
            length[0xDE] = Dynamic; // Update Mobile Status
            length[0xDF] = Dynamic; // Buff/Debuff System
            length[0xE0] = Dynamic; // Bug Report (KR)
            length[0xE1] = 0x0009; // Character List Update (KR)
            length[0xE2] = 0x000A; // Mobile status/Animation update (KR)
            length[0xE3] = Dynamic; //KR Encryption Request
            length[0xE4] = Dynamic; //KR Encryption Response
            length[0xE5] = Dynamic; //Display Waipoint
            length[0xE6] = 0x0005; // Hide Waypoint
            length[0xE7] = 0x000C; // Continue Highlight KR UI Element - ONLY KR
            length[0xE8] = 0x000D; // Remove Highlight KR UO Element - ONLY KR
            length[0xE9] = 0x005F; // Toggle Highlight KR UI Element - ONLY KR
            length[0xEA] = 0x0003; // Enable KR Hotbar - ONLY KR
            length[0xEB] = 0x000B; // Report Use KR Hotbar Icon
            length[0xEC] = Dynamic; // Equip Items KR Macro
            length[0xED] = Dynamic; // Unequip Items KR Macro
            length[0xEE] = 0x000A; // Unknown
            length[0xEF] = 0x0015; // LoginServerSeed
            length[0xF0] = Dynamic; // Unknown
            length[0xF1] = 0x0014; // Unknown
            length[0xF2] = 0x0019; // Unknown
            length[0xF3] = 0x0018; // Object Information
            length[0xF4] = Dynamic; // Crash Report Packer
            length[0xF5] = 0x0015; // New Map Details Packet
            length[0xF6] = Dynamic; // Boat Moving Packet
            length[0xF7] = Dynamic; // Packet Conteiner Packet
            length[0xF8] = 0x006A; // New Character Creation
        }
    }
}
