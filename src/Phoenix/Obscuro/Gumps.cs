﻿using Phoenix.Communication;
using Phoenix.WorldData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Phoenix.NewPhoenix
{
    internal static class Gumps
    {

        public enum AutoOpenedGump : byte
        {
            Nothing = 255,
            Opened = 0,
            AutoOpened = 1
        }

        internal static readonly GumpPublicEvent gumpsCallbacks = new GumpPublicEvent();

        private static Boolean AccessGump = false;
        private static Boolean DetailedGump = false;
        private static Boolean AutoOpenGump = false;
        private static AutoOpenedGump AutoOpening = AutoOpenedGump.Nothing;
        private static UInt32 GumpButton = 0;
        private static GumpTextEntry[] GumpEntry;
        private static GumpSwitches[] GumpSwitch;

        private static InfoGump informationGump;
        private static InfoGumpSended sendedGump;

        internal static void Init()
        {
            Core.RegisterServerMessageCallback(0xDD, new MessageCallback(GenericGump));
            Core.RegisterServerMessageCallback(0xB0, new MessageCallback(OldGenericGump));
            Core.RegisterClientMessageCallback(0xB1, new MessageCallback(ClientSendGump));
        }

        public static void GumpSend(UInt32 itemID, UInt32 button)
        {
            AccessGump = true;
            DetailedGump = false;
            AutoOpenGump = false;
            AutoOpening = AutoOpenedGump.Nothing;
            GumpButton = button;
            GumpEntry = null;
            GumpSwitch = null; 
            Core.SendToServer(Phoenix.Communication.PacketBuilder.ObjectDoubleClick(itemID));
        }

        public static void GumpSend(UInt32 itemID, UInt32 button, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches)
        {
            AccessGump = true;
            DetailedGump = true;
            AutoOpenGump = false;
            AutoOpening = AutoOpenedGump.Nothing;
            GumpButton = button;
            GumpEntry = gumpTextEntry;
            GumpSwitch = gumpSwitches;
            Core.SendToServer(Phoenix.Communication.PacketBuilder.ObjectDoubleClick(itemID));
        }

        public static void GumpSend(UInt32 itemID, UInt32 button, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches, Boolean autoOpeningGump)
        {
            AccessGump = true;
            DetailedGump = true;
            AutoOpenGump = autoOpeningGump;
            if (AutoOpenGump)
                AutoOpening = AutoOpenedGump.Opened;
            else
                AutoOpening = AutoOpenedGump.Nothing;
            GumpButton = button;
            GumpEntry = gumpTextEntry;
            GumpSwitch = gumpSwitches;
            Core.SendToServer(Phoenix.Communication.PacketBuilder.ObjectDoubleClick(itemID));
        }

        public static void GumpSendIncoming(UInt32 button, InfoGump infoGump)
        {
            GumpSendIncoming(button, null, null, infoGump);
        }

        public static void GumpSendIncoming(UInt32 button, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches, InfoGump infoGump)
        {
            GumpSendIncoming(button, gumpTextEntry, gumpSwitches, infoGump, false);
        }

        public static void GumpSendIncoming(UInt32 button, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches, InfoGump infoGump, Boolean autoOpeningGump)
        {
            AutoOpenGump = autoOpeningGump;
            if (AutoOpenGump)
                AutoOpening = AutoOpenedGump.Opened;
            else
                AutoOpening = AutoOpenedGump.Nothing;
            GumpButton = button;
            GumpEntry = gumpTextEntry;
            GumpSwitch = gumpSwitches;
            Core.SendToServer(GumpTriggerDetailedSend(infoGump));
        }

        public static InfoGump infoGump
        {
            get { return informationGump; }
            set { informationGump = value; }
        }

        public static void ShowGumpInfo(Boolean detailed)
        {
            Notepad.Clear();
            List<String> gumpInfo = new List<String>();
            gumpInfo.Add("Packet: 0xB0");
            gumpInfo.Add("PacketSize: 0x" + informationGump.PacketSize.ToString("X2"));
            gumpInfo.Add("GumpSerial: 0x" + informationGump.GumpSerial.ToString("X2"));
            gumpInfo.Add("GumpTypeID: 0x" + informationGump.GumpTypeID.ToString("X2"));
            gumpInfo.Add("PositionX: " + informationGump.GumpX);
            gumpInfo.Add("PositionY: " + informationGump.GumpY);
            if (detailed)
            {
                gumpInfo.Add("-----------------------------------------");
                String[] gumpData = informationGump.GumpData.Split('}');
                foreach(String data in gumpData)
                    gumpInfo.Add(data.Replace("{", ""));
                gumpInfo.Add("-----------------------------------------");
            }
            foreach (GumpTextEntry entry in informationGump.GumpTextEntry)
            {
                gumpInfo.Add("ID: " + entry.EntryID);
                gumpInfo.Add("Text: " + entry.Text);
            }

            foreach (String txt in gumpInfo)
                Notepad.WriteLine(txt);
            Notepad.Open();
        }

        public static InfoGumpSended infoSendedGump
        {
            get { return sendedGump; }
            set { sendedGump = value; }
        }

        public static void ShowSendedGump()
        {
            Notepad.Clear();
            List<String> gump = new List<String>();
            gump.Add("Packet: 0xB1");
            gump.Add("PacketSize: 0x" + sendedGump.PacketSize.ToString("X2"));
            gump.Add("GumpSerial: 0x" + sendedGump.GumpID.ToString("X2"));
            gump.Add("GumpTypeID: 0x" + sendedGump.GumpSerial.ToString("X2"));
            gump.Add("ButtonID: " + sendedGump.ButtonID);
            gump.Add("SwitchCount: " + sendedGump.SwitchCount);
            for (int i = 0; i < sendedGump.SwitchCount; i++)
                gump.Add("  Switch[" + i + "]: " + sendedGump.GumpSwitches[i].SwitchID);
            gump.Add("TextEntryCount:" + sendedGump.TextCount);
            for (int i = 0; i < sendedGump.TextCount; i++)
            {
                gump.Add("  ID: " + sendedGump.GumpTextEntry[i].EntryID + " Text: " + sendedGump.GumpTextEntry[i].Text);
            }
            foreach (String txt in gump)
                Notepad.WriteLine(txt);
            Notepad.Open();
        }
        

        private static CallbackResult OldGenericGump(byte[] data, CallbackResult prevState)
        {
            informationGump = new InfoGump();
            lock(World.SyncRoot)
            {
                if (data[0] != 0xB0) throw new Exception("Invalid packet passed to OldGenericGump.");

                informationGump.PacketSize = ByteConverter.BigEndian.ToUInt16(data, 1);
                informationGump.GumpSerial = ByteConverter.BigEndian.ToUInt32(data, 3);
                informationGump.GumpTypeID = ByteConverter.BigEndian.ToUInt32(data, 7);
                informationGump.GumpX = ByteConverter.BigEndian.ToUInt32(data, 11);
                informationGump.GumpY = ByteConverter.BigEndian.ToUInt32(data, 15);
                int DLen = ByteConverter.BigEndian.ToUInt16(data, 19);
                informationGump.GumpData = ByteConverter.BigEndian.ToAsciiString(data, 21, DLen);
                int offset = 21 + DLen;
                informationGump.TextLines = ByteConverter.BigEndian.ToUInt16(data, offset);
                if (informationGump.TextLines == 0) informationGump.GumpTextEntry = new GumpTextEntry[0];
                if (informationGump.TextLines > 0) 
                    informationGump.GumpTextEntry = new GumpTextEntry[informationGump.TextLines];
                offset += 2;
                for (int i = 0; i < informationGump.TextLines; i++)
                {
                    ushort Len = ByteConverter.BigEndian.ToUInt16(data, offset);
                    offset += 2;
                    string Text = ByteConverter.BigEndian.ToUnicodeString(data, offset, Len * 2);
                    offset += Len * 2;
                    GumpTextEntry entry = new GumpTextEntry((ushort)i, Text);
                    informationGump.GumpTextEntry[i] = entry;
                }

                gumpsCallbacks.InvokeAsync(null, new GumpEventArgs(informationGump));
                Thread.Sleep(50);

                // Wait for clientDataCallback
                if (informationGump.ReturnedData != null)
                {
                    int tmpMs = 0;
                    while (!informationGump.ReturnedData.CallbackReturned)
                    {
                        if ((tmpMs > informationGump.ReturnedData.MaxWaitMiliseconds) || informationGump.ReturnedData.CallbackReturned)
                            break;
                        tmpMs += 100;
                        Thread.Sleep(100);
                    }

                    if (informationGump.ReturnedData.CallbackReturned)
                    {
                        Byte[] sendTrigger = GumpTriggerDetailedSend(informationGump);
                        Core.SendToServer(sendTrigger);
                        informationGump.ReturnedData.CallbackReturned = false;
                        return CallbackResult.Sent;
                    }
                    else
                        return CallbackResult.Normal;
                }

                if (AccessGump)
                {
                    if (!AutoOpenGump)
                    {
                        Byte[] sendTrigger;
                        if (DetailedGump)
                            sendTrigger = GumpTriggerDetailedSend(informationGump);
                        else
                            sendTrigger = GumpTriggerSend(informationGump);
                        Core.SendToServer(sendTrigger);
                        DetailedGump = false;
                        AccessGump = false;
                        return CallbackResult.Sent;
                    }
                    else
                    {
                        switch(AutoOpening)
                        {
                            case AutoOpenedGump.Nothing:
                                return CallbackResult.Normal;
                            
                            case AutoOpenedGump.AutoOpened:
                                AutoOpening = AutoOpenedGump.Nothing;
                                Core.SendToServer(CreateCloseGumpPacket(data));
                                AccessGump = false;
                                return CallbackResult.Sent;

                            case AutoOpenedGump.Opened:
                                AutoOpening = AutoOpenedGump.AutoOpened;
                                Byte[] sendTrigger;
                                if (DetailedGump)
                                    sendTrigger = GumpTriggerDetailedSend(informationGump);
                                else
                                    sendTrigger = GumpTriggerSend(informationGump);
                                Core.SendToServer(sendTrigger);
                                DetailedGump = false;
                                return CallbackResult.Sent;
                        }
                    }
                }
                return CallbackResult.Normal;
            }
        }

        private static byte[] CreateCloseGumpPacket(byte[] data)
        {
            PacketWriter pw = new PacketWriter(0xB1);
            pw.WriteBlockSize();
            pw.Write(ByteConverter.BigEndian.ToUInt32(data, 3));
            pw.Write(ByteConverter.BigEndian.ToUInt32(data, 7));
            pw.Write(0);
            pw.Write(0);
            pw.Write(0);
            return pw.GetBytes();
        }

        private static CallbackResult GenericGump(byte[] data, CallbackResult prevState)
        {
            informationGump = new InfoGump();
            lock(World.SyncRoot)
            {
                if (data[0] != 0xDD) throw new Exception("Invalid packet passed to GenericGump.");
                
                informationGump.PacketSize = ByteConverter.BigEndian.ToUInt16(data, 1);
                informationGump.GumpSerial = ByteConverter.BigEndian.ToUInt32(data, 3);
                informationGump.GumpTypeID = ByteConverter.BigEndian.ToUInt32(data, 7);
                informationGump.GumpX = ByteConverter.BigEndian.ToUInt32(data, 11);
                informationGump.GumpY = ByteConverter.BigEndian.ToUInt32(data, 15);
                int CLen = ByteConverter.BigEndian.ToInt32(data, 19) - 4;
                int DLen = ByteConverter.BigEndian.ToInt32(data, 23);
                byte[] CData = ReadBytes(data, CLen, 27);
                byte[] DData = new byte[DLen];
                if (ExternDLL.Zlib.uncompress(DData, ref DLen, CData, CLen) != ExternDLL.ZLibError.Z_OK)
                    UO.PrintError("GenericGump Error");
                else
                {
                    informationGump.GumpData = Encoding.ASCII.GetString(DData);
                    int index = 27 + CLen;
                    informationGump.TextLines = ByteConverter.BigEndian.ToInt32(data, index);
                    index += 4;
                    int CTextLen = ByteConverter.BigEndian.ToInt32(data, index) - 4;
                    index += 4;
                    int DTextLen = ByteConverter.BigEndian.ToInt32(data, index);
                    index += 4;
                    byte[] DText = new byte[DTextLen];
                    if (informationGump.TextLines > 0 && DTextLen > 0)
                    {
                        byte[] CTextData = ReadBytes(data, CTextLen, index);
                        ExternDLL.Zlib.uncompress(DText, ref DTextLen, CTextData, CTextLen);
                        int _index = 0;
                        informationGump.GumpTextEntry = new GumpTextEntry[informationGump.TextLines];
                        for (int i = 0; i < informationGump.TextLines; i++)
                        {
                            int length = DText[_index] * 256 + DText[_index + 1];
                            _index += 2;
                            byte[] b = new byte[length * 2];
                            Array.Copy(DText, _index, b, 0, length * 2);
                            _index += length * 2;
                            string text = Encoding.BigEndianUnicode.GetString(b);
                            GumpTextEntry entry = new GumpTextEntry((ushort)i, text);
                            informationGump.GumpTextEntry[i] = entry;
                        }
                    }
                    else
                        informationGump.GumpTextEntry = new GumpTextEntry[0];
                }

                gumpsCallbacks.InvokeAsync(null, new GumpEventArgs(informationGump));
                Thread.Sleep(50);
                
                // Wait for clientDataCallback
                if (informationGump.ReturnedData != null)
                {
                    int tmpMs = 0;
                    while (!informationGump.ReturnedData.CallbackReturned)
                    {
                        if ((tmpMs > informationGump.ReturnedData.MaxWaitMiliseconds) || informationGump.ReturnedData.CallbackReturned)
                            break;
                        tmpMs += 100;
                        Thread.Sleep(100);
                    }

                    if (informationGump.ReturnedData.CallbackReturned)
                    {
                        Byte[] sendTrigger = GumpTriggerDetailedSend(informationGump);
                        Core.SendToServer(sendTrigger);
                        informationGump.ReturnedData.CallbackReturned = false;
                        return CallbackResult.Sent;
                    }
                    else
                        return CallbackResult.Normal;
                }

                if (AccessGump)
                {
                    if (!AutoOpenGump)
                    {
                        Byte[] sendTrigger;
                        if (DetailedGump)
                            sendTrigger = GumpTriggerDetailedSend(informationGump);
                        else
                            sendTrigger = GumpTriggerSend(informationGump);
                        Core.SendToServer(sendTrigger);
                        DetailedGump = false;
                        AccessGump = false;
                        return CallbackResult.Sent;
                    }
                    else
                    {
                        switch (AutoOpening)
                        {
                            case AutoOpenedGump.Nothing:
                                return CallbackResult.Normal;

                            case AutoOpenedGump.AutoOpened:
                                AutoOpening = AutoOpenedGump.Nothing;
                                Core.SendToServer(CreateCloseGumpPacket(data));
                                AccessGump = false;
                                return CallbackResult.Sent;

                            case AutoOpenedGump.Opened:
                                AutoOpening = AutoOpenedGump.AutoOpened;
                                Byte[] sendTrigger;
                                if (DetailedGump)
                                    sendTrigger = GumpTriggerDetailedSend(informationGump);
                                else
                                    sendTrigger = GumpTriggerSend(informationGump);
                                Core.SendToServer(sendTrigger);
                                DetailedGump = false;
                                return CallbackResult.Sent;
                        }
                    }
                }

                return CallbackResult.Normal;
            }
        }

        private static CallbackResult ClientSendGump(byte[] data, CallbackResult prevState)
        {
            sendedGump = new InfoGumpSended();
            lock (World.SyncRoot)
            {
                if (data[0] != 0xB1) throw new Exception("Invalid packet passed to ClientSendGump.");
                PacketReader pr = new PacketReader(data);
                pr.Skip(1); // skip packetID
                sendedGump.PacketSize = pr.ReadUInt16();
                sendedGump.GumpID = pr.ReadUInt32();
                sendedGump.GumpSerial = pr.ReadUInt32();
                sendedGump.ButtonID = pr.ReadUInt32();
                sendedGump.SwitchCount = pr.ReadUInt32();
                sendedGump.GumpSwitches = new GumpSwitches[sendedGump.SwitchCount];
                for (int i = 0; i < sendedGump.SwitchCount; i++)
                    sendedGump.GumpSwitches[i] = new GumpSwitches(pr.ReadUInt32());
                sendedGump.TextCount = pr.ReadUInt32();
                sendedGump.GumpTextEntry = new GumpTextEntry[sendedGump.TextCount];
                for (int i = 0; i < sendedGump.TextCount; i++)
                {
                    ushort id = pr.ReadUInt16();
                    ushort len = pr.ReadUInt16();
                    string text = pr.ReadUnicodeString(len * 2);
                    sendedGump.GumpTextEntry[i] = new GumpTextEntry(id, text); 
                }
                return CallbackResult.Normal;
            }
        }

        private static Byte[] GumpTriggerSend(InfoGump infoGump)
        {
            Byte[] tmp = new Byte[23];
            ByteConverter.BigEndian.ToBytes((Byte)0xB1, tmp, 0);
            ByteConverter.BigEndian.ToBytes((UInt16)0x17, tmp, 1);
            ByteConverter.BigEndian.ToBytes((UInt32)infoGump.GumpSerial, tmp, 3);
            ByteConverter.BigEndian.ToBytes((UInt32)infoGump.GumpTypeID, tmp, 7);
            ByteConverter.BigEndian.ToBytes((UInt32)GumpButton, tmp, 11);
            ByteConverter.BigEndian.ToBytes((UInt32)0, tmp, 15);
            ByteConverter.BigEndian.ToBytes((UInt32)0, tmp, 19);
            return tmp;
        }

        public static Byte[] GumpTriggerDetailedSend(InfoGump infoGump)
        {
            PacketWriter pw = new PacketWriter(0xB1);
            pw.WriteBlockSize();

            if (informationGump.ReturnedData != null)
            {
                pw.Write((UInt32)infoGump.ReturnedData.GumpSerial);
                pw.Write((UInt32)infoGump.ReturnedData.GumpTypeID);
                pw.Write((UInt32)infoGump.ReturnedData.Button);
                if (infoGump.ReturnedData.Switches != null)
                {
                    pw.Write((UInt32)infoGump.ReturnedData.Switches.Length);
                    for (int i = 0; i < infoGump.ReturnedData.Switches.Length; i++)
                        pw.Write((UInt32)infoGump.ReturnedData.Switches[i].SwitchID);
                }
                else
                    pw.Write((UInt32)0);
                if (infoGump.ReturnedData.TextEntry != null)
                {
                    pw.Write((UInt32)infoGump.ReturnedData.TextEntry.Length);
                    for (int i = 0; i < infoGump.ReturnedData.TextEntry.Length; i++)
                    {
                        pw.Write((UInt16)infoGump.ReturnedData.TextEntry[i].EntryID);
                        pw.Write((UInt16)infoGump.ReturnedData.TextEntry[i].Text.Length);
                        pw.WriteUnicodeString(infoGump.ReturnedData.TextEntry[i].Text);
                    }
                }
                else
                    pw.Write((UInt32)0);
            }
            else
            {
                pw.Write((UInt32)infoGump.GumpSerial);
                pw.Write((UInt32)infoGump.GumpTypeID);
                pw.Write((UInt32)GumpButton);
                if (GumpSwitch != null)
                {
                    pw.Write((UInt32)GumpSwitch.Length);
                    for (int i = 0; i < GumpSwitch.Length; i++)
                        pw.Write((UInt32)GumpSwitch[i].SwitchID);
                }
                else
                    pw.Write((UInt32)0);
                if (GumpEntry != null)
                {
                    pw.Write((UInt32)GumpEntry.Length);
                    for (int i = 0; i < GumpEntry.Length; i++)
                    {
                        pw.Write((UInt16)GumpEntry[i].EntryID);
                        pw.Write((UInt16)GumpEntry[i].Text.Length);
                        pw.WriteUnicodeString(GumpEntry[i].Text);
                    }
                }
                else
                    pw.Write((UInt32)0);
            }
            return pw.GetBytes();
        }

        private static byte[] ReadBytes(byte[] data, int dataLen, int indexLen)
        {
            byte[] newData = new byte[dataLen];
            for (int i = 0; i < dataLen; i++)
            {
                newData[i] = data[indexLen];
                indexLen++;
            }
            return newData;
        }
    }

    public class InfoGump
    {
        public GumpDataCallback ReturnedData { get; set; }
        public ushort PacketSize { get; set; }
        public uint GumpSerial { get; set; }
        public uint GumpTypeID { get; set; }
        public uint GumpX { get; set; }
        public uint GumpY { get; set; }
        public string GumpData { get; set; }
        public int TextLines { get; set; }
        public GumpTextEntry[] GumpTextEntry { get; set; }
        public GumpSwitches[] GumpSwitechs { get; set; }

        public InfoGump()
        {
            ReturnedData = null;
        }
    }

    public class GumpDataCallback
    {
        public uint GumpSerial { get; set; }
        public uint GumpTypeID { get; set; }
        public uint Button { get; set; }
        public GumpTextEntry[] TextEntry { get; set; }
        public GumpSwitches[] Switches { get; set; }

        public int MaxWaitMiliseconds { get; set; }
        public bool CallbackReturned { get; set; }

        public GumpDataCallback()
        {
            this.MaxWaitMiliseconds = 0;
            this.CallbackReturned = false;
            this.Button = 0xFFFFFFFF;
        }
        
    }

    public class InfoGumpSended
    {
        public ushort PacketSize { get; set; }
        public uint GumpSerial { get; set; }
        public uint GumpID { get; set; }
        public uint ButtonID { get; set; }
        public uint SwitchCount { get; set; }
        public GumpSwitches[] GumpSwitches { get; set; }
        public uint TextCount { get; set; }
        public GumpTextEntry[] GumpTextEntry { get; set; }
    }

    public class GumpSwitches
    {
        public uint SwitchID;

        public GumpSwitches(uint switchID)
        {
            this.SwitchID = switchID;
        }
    }

    public class GumpTextEntry
    {
        public ushort EntryID;
        public string Text;

        public GumpTextEntry(ushort id, string s)
        {
            this.EntryID = id;
            this.Text = s;
        }
    }
}
