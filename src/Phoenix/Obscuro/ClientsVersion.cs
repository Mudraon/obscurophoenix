﻿using System;
using System.Collections.Generic;
using System.Text;
using Phoenix.Communication;
using System.Diagnostics;

namespace Phoenix.NewPhoenix
{
    public static class ClientsVersion
    {
        public static Int32 comboBoxClientVer;

        private static Boolean[] SetMoreVersion()
        {
            Boolean[] tmp = new Boolean[] { false, false, false, false, false };
            int version = comboBoxClientVer;

            for (int i = 1; i <= 5; i++)
            {
                if (i <= version)
                    tmp[i - 1] = true;
            }
            return tmp;
        }

        public static void SetVersionPacket()
        {
            Boolean[] isActive = SetMoreVersion();

            // if ver 1 do nothing
            /*if (isActive[0])
            {
                Packetlenght.SetNewLength(0x08, 0x0E);
                Packetlenght.SetNewLength(0x25, 0x14);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x010A);
                Packetlenght.SetNewLength(0xB9, 0x05);
                Trace.WriteLine(String.Format("Packet lower 6.0.1.7 set!"), "Client Version");
            }*/

            if (isActive[1]) //if (comboBoxClientVer == 2) // Since 6.0.1.7
            {
                Packetlenght.SetNewLength(0x08, 0x0F);
                Packetlenght.SetNewLength(0x25, 0x15);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x07);
                Packetlenght.SetNewLength(0xB9, 0x03);
                Trace.WriteLine(String.Format("Packet since 6.0.1.7 set!"), "Client Version");
            }
            else
            {
                Packetlenght.SetNewLength(0x08, 0x0E);
                Packetlenght.SetNewLength(0x25, 0x14);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x010A);
                Packetlenght.SetNewLength(0xB9, 0x05);
                Trace.WriteLine(String.Format("Packet lower 6.0.1.7 set!"), "Client Version");
            }

            if (isActive[2]) //if (comboBoxClientVer == 3) // Since 6.0.14.2
            {
                Packetlenght.SetNewLength(0xB9, 0x05);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x07);
                Packetlenght.SetNewLength(0xB9, 0x05);
                Trace.WriteLine(String.Format("Packet since 6.0.14.2 set!"), "Client Version");
            }
            else
            {
                Packetlenght.SetNewLength(0xB9, 0x03);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x010A);
                Packetlenght.SetNewLength(0xB9, 0x03);
                Trace.WriteLine(String.Format("Packet lower 6.0.14.2 set!"), "Client Version");
            }

            if (isActive[3]) //if (comboBoxClientVer == 4) // Since 7.0.9
            {
                Packetlenght.SetNewLength(0x24, 0x09);
                Packetlenght.SetNewLength(0xF3, 0x1A);
                Packetlenght.SetNewLength(0x99, 0x1E);
                Packetlenght.SetNewLength(0xBA, 0x0A);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x07);
                Packetlenght.SetNewLength(0xB9, 0x05);
                Trace.WriteLine(String.Format("Packet since 7.0.9 set!"), "Client Version");
            }
            else
            {
                Packetlenght.SetNewLength(0x24, 0x07);
                Packetlenght.SetNewLength(0xF3, 0x18);
                Packetlenght.SetNewLength(0x99, 0x1A);
                Packetlenght.SetNewLength(0xBA, 0x06);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x010A);
                Packetlenght.SetNewLength(0xB9, 0x03);
                Trace.WriteLine(String.Format("Packet lower 7.0.9 set!"), "Client Version");
            }

            if (isActive[4]) //if (comboBoxClientVer == 5) // Since 7.0.35.0
            {
                Packetlenght.SetNewLength(0xF3, 0x1A);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x07);
                Packetlenght.SetNewLength(0xB9, 0x05);
                Trace.WriteLine(String.Format("Packet since 7.0.35.0 set!"), "Client Version");
            }
            else
            {
                Packetlenght.SetNewLength(0xF3, 0x18);
                Packetlenght.SetNewLength(0x9B, 0x0102);
                Packetlenght.SetNewLength(0x0B, 0x010A);
                Packetlenght.SetNewLength(0xB9, 0x03);
                Trace.WriteLine(String.Format("Packet lower 7.0.35.0 set!"), "Client Version");
            }


        }

    }
}
