﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.NewPhoenix
{
    public class AgentsEventArgs : EventArgs
    {
        private AgentsInfo FAgentsInfo;

        public AgentsEventArgs(AgentsInfo aAgentsInfo)
        {
            this.FAgentsInfo = aAgentsInfo;
        }

        public AgentsInfo Info
        {
            get { return this.FAgentsInfo; }
        }
    }

    public delegate void AgentsEventHandler(object sender, AgentsEventArgs e);

    internal class AgentsPublicEvent : PublicEvent<AgentsEventHandler, AgentsEventArgs>
    {
    }
}