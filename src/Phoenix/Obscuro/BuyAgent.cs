﻿using Phoenix.Communication;
using Phoenix.WorldData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.NewPhoenix
{

    public class BuyItem
    {
        public Serial Serial { get; set; }
        public Graphic Graphic { get; set; }
        public UInt32 Price { get; set; }
        public UInt32 LimitAmount { get; set; }

        public BuyItem(Serial serial, Graphic graphic, uint price, uint limitAmount)
        {
            this.Serial = serial;
            this.Graphic = graphic;
            this.Price = price;
            this.LimitAmount = limitAmount;
        }

        public BuyItem(Serial serial, Graphic graphic, uint price)
        {
            this.Serial = serial;
            this.Graphic = graphic;
            this.Price = price;
            this.LimitAmount = 0xFFFF;
        }
    }

    public class BuyAgentInformation
    {
        public List<BuyItem> Item2Buy { get; set; }
        public Serial Vendor { get; set; }
        public UInt16[] BuyList { get; set; }
        public UInt32[] PriceList { get; set; }
        public UInt32[] AmountLimitList { get; set; }
        public Boolean ExactValue { get; set; }
        public Boolean isBuyLimit = false;
    }

    public static class BuyAgent
    {
        private static BuyAgentInformation buyInfo;

        internal static void Init()
        {
            Core.RegisterServerMessageCallback(0x78, new MessageCallback(EqMob)); // Get vendor serial
            Core.RegisterServerMessageCallback(0x3C, new MessageCallback(ContainerContents)); // Get items to buy
            Core.RegisterServerMessageCallback(0x74, new MessageCallback(ShopData)); // Get items prices
            buyInfo = new BuyAgentInformation();
            buyInfo.Item2Buy = new List<BuyItem>();
            buyInfo.ExactValue = false;
        }

        public static void Buy(ushort[] m_buyList)
        {
            buyInfo.BuyList = m_buyList;
            buyInfo.AmountLimitList = null;
            buyInfo.PriceList = null;
            buyInfo.ExactValue = false;
            Engine();
        }

        public static void Buy(ushort[] m_buyList, uint[] m_priceList, bool exactValue)
        {
            buyInfo.BuyList = m_buyList;
            buyInfo.AmountLimitList = null;
            buyInfo.PriceList = m_priceList;
            buyInfo.ExactValue = exactValue;
            Engine();
        }

        public static void Buy(ushort[] m_buyList, uint[] m_amountLimit)
        {
            buyInfo.BuyList = m_buyList;
            buyInfo.AmountLimitList = m_amountLimit;
            buyInfo.PriceList = null;
            buyInfo.ExactValue = false;
            Engine();
        }

        public static Boolean isBuyLimit()
        {
            return buyInfo.isBuyLimit;
        }

        private static void Engine()
        {
            List<BuyItem> m_buy = new List<BuyItem>();
            buyInfo.isBuyLimit = false;
            foreach (BuyItem _buyItem in buyInfo.Item2Buy)
            {
                for (int i = 0; i < buyInfo.BuyList.Length; i++)
                {
                    //Just only buyList
                    if (buyInfo.PriceList == null)
                    {
                        // Pokud to má limit aby se pro to vracel a koupil znova!
                        if (buyInfo.AmountLimitList != null && buyInfo.AmountLimitList.Length > 0)
                        {
                            UOItem m_vendorItem = new UOItem(_buyItem.Serial);
                            if (buyInfo.BuyList[i] == _buyItem.Graphic && buyInfo.AmountLimitList[i] >= m_vendorItem.Amount)
                            {
                                m_buy.Add(new BuyItem(_buyItem.Serial, _buyItem.Graphic, _buyItem.Price, m_vendorItem.Amount));
                            }
                            else if (buyInfo.BuyList[i] == _buyItem.Graphic && buyInfo.AmountLimitList[i] < m_vendorItem.Amount)
                            {
                                m_buy.Add(new BuyItem(_buyItem.Serial, _buyItem.Graphic, _buyItem.Price, buyInfo.AmountLimitList[i]));
                                buyInfo.isBuyLimit = true;
                            }
                        }
                        else
                            if (buyInfo.BuyList[i] == _buyItem.Graphic)
                                m_buy.Add(new BuyItem(_buyItem.Serial, _buyItem.Graphic, _buyItem.Price));
                    }

                    //Just priceList and exactValue
                    if (buyInfo.PriceList != null)
                    {
                        if (!buyInfo.ExactValue)
                        {
                            if (buyInfo.BuyList[i] == _buyItem.Graphic && buyInfo.PriceList[i] >= _buyItem.Price)
                                m_buy.Add(new BuyItem(_buyItem.Serial, _buyItem.Graphic, _buyItem.Price));
                        }
                        else
                        {
                            if (buyInfo.BuyList[i] == _buyItem.Graphic && buyInfo.PriceList[i] == _buyItem.Price)
                                m_buy.Add(new BuyItem(_buyItem.Serial, _buyItem.Graphic, _buyItem.Price));
                        }
                    }
                }
            }
            if (buyInfo.ExactValue) buyInfo.ExactValue = false;
            Core.SendToServer(BuyItemsBuild(buyInfo.Vendor, m_buy));
        }

        private static byte[] BuyItemsBuild(Serial vendor, List<BuyItem> m_buy)
        {
            PacketWriter pw = new PacketWriter(0x3B);
            pw.Write((ushort)(8 + (m_buy.Count * 7)));
            pw.Write((uint)vendor);
            pw.Write((byte)0x02); // flag (0x00 - no items following|0x02 - items following)
            foreach (BuyItem m_item in m_buy)
            {
                UOItem item = new UOItem(m_item.Serial);
                pw.Write((byte)0x1A); // Layer - shopLayer
                pw.Write((Serial)item.Serial);
                if (m_item.LimitAmount == 0xFFFF)
                    pw.Write((ushort)item.Amount);
                else
                    pw.Write((ushort)m_item.LimitAmount);
            }
            return pw.GetBytes();
        }

        private static CallbackResult ShopData(byte[] data, CallbackResult prevState)
        {
            if (data[0] != 0x74) throw new NotImplementedException();

            PacketReader pr = new PacketReader(data);
            byte Packet = pr.ReadByte();
            ushort Size = pr.ReadUInt16();
            uint Vendor = pr.ReadUInt32();
            byte Number = pr.ReadByte();
            for (int i = 0; i < Number; i++)
            {
                uint Price = pr.ReadUInt32();
                byte Length = pr.ReadByte();
                string Name = pr.ReadAnsiString(Length);
                buyInfo.Item2Buy[i].Price = Price;
            }
            return CallbackResult.Normal;

        }

        private static CallbackResult ContainerContents(byte[] data, CallbackResult prevState)
        {
            if (data[0] != 0x3C) throw new NotImplementedException();

            buyInfo.Item2Buy.Clear();
            ushort itemCount = ByteConverter.BigEndian.ToUInt16(data, 3);
            for (int i = 0; i < itemCount; i++)
            {
                int index = 19;
                if (NewPhoenix.ClientsVersion.comboBoxClientVer > 1) index = 20;
                uint itemSerial = ByteConverter.BigEndian.ToUInt32(data, 5 + (index * i));
                ushort itemGraphic = ByteConverter.BigEndian.ToUInt16(data, 9 + (index * i));
                buyInfo.Item2Buy.Add(new BuyItem(itemSerial, itemGraphic, 0));
            }
            return CallbackResult.Normal;
        }

        private static CallbackResult EqMob(byte[] data, CallbackResult prevState)
        {
            if (data[0] != 0x78) throw new NotImplementedException();

            Serial vendor = ByteConverter.BigEndian.ToUInt32(data, 3);
            buyInfo.Vendor = vendor;
            return CallbackResult.Normal;
        }

    }
}
