﻿using Phoenix.Communication;
using Phoenix.WorldData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.NewPhoenix
{

    public class SellItem
    {
        public Serial Serial { get; set; }
        public Graphic Graphic { get; set; }

        public SellItem(Serial serial, Graphic graphic)
        {
            this.Serial = serial;
            this.Graphic = graphic;
        }
    }

    public class SellAgentInformation
    {
        public List<SellItem> Items2Sell { get; set; }
        public Serial Vendor { get; set; }
        public UInt16[] SellList { get; set; }
    }

    public static class SellAgent
    {
        private static SellAgentInformation sellInfo;

        internal static void Init()
        {
            Core.RegisterServerMessageCallback(0x9E, new MessageCallback(ShopSell)); //Get vendor serial & items to sell
            sellInfo = new SellAgentInformation();
            sellInfo.Items2Sell = new List<SellItem>();
        }

        public static void Sell(ushort[] m_sellList)
        {
            sellInfo.SellList = m_sellList;
            Engine();
        }

        private static void Engine()
        {
            List<SellItem> m_sell = new List<SellItem>();
            foreach (SellItem _sellItem in sellInfo.Items2Sell)
                for (int i = 0; i < sellInfo.SellList.Length; i++)
                    if (sellInfo.SellList[i] == _sellItem.Graphic)
                        m_sell.Add(new SellItem(_sellItem.Serial, _sellItem.Graphic));
            Core.SendToServer(SellItemsBuild(sellInfo.Vendor, m_sell));
        }

        private static byte[] SellItemsBuild(Serial vendor, List<SellItem> m_sell)
        {
            ushort length;
            if (m_sell.Count > 80)
                length = 80;
            else
                length = (ushort)m_sell.Count;

            PacketWriter pw = new PacketWriter(0x9F);
            pw.Write((ushort)(9 + (length * 6)));
            pw.Write((uint)vendor);
            pw.Write(length);
            foreach (SellItem m_item in m_sell)
            {
                UOItem item = new UOItem(m_item.Serial);
                pw.Write((Serial)item.Serial);
                pw.Write((ushort)item.Amount);
            }
            return pw.GetBytes();
        }

        private static CallbackResult ShopSell(byte[] data, CallbackResult prevState)
        {
            if (data[0] != 0x9E) throw new NotImplementedException();

            sellInfo.Items2Sell.Clear();
            PacketReader pr = new PacketReader(data);
            byte Packet = pr.ReadByte();
            ushort Size = pr.ReadUInt16();
            Serial Vendor = pr.ReadUInt32();
            sellInfo.Vendor = Vendor;
            ushort NumItems = pr.ReadUInt16();
            for (int i = 0; i < NumItems; i++)
            {
                Serial itemSerial = pr.ReadUInt32();
                Graphic itemGraphic = pr.ReadUInt16();
                ushort hue = pr.ReadUInt16();
                ushort amount = pr.ReadUInt16();
                ushort price = pr.ReadUInt16();
                ushort length = pr.ReadUInt16();
                string name = pr.ReadAnsiString(length);
                sellInfo.Items2Sell.Add(new SellItem(itemSerial, itemGraphic));
            }
            return CallbackResult.Normal;
        }
    }
}
