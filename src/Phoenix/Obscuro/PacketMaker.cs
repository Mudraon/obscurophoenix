﻿using Phoenix.Communication;
using Phoenix.WorldData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.NewPhoenix
{
    public static class PacketMaker
    {
        public static byte[] GumpMenuSelection(uint ID, uint button, uint[] switches, GumpTextEntry[] enteries)
        {
            PacketWriter pw = new PacketWriter(0xB1);
            pw.WriteBlockSize();
            pw.Write(ID);
            pw.Write(Gumps.infoGump.GumpTypeID);
            pw.Write(button);
            pw.Write(switches.Length);
            for (int i = 0; i < switches.Length; i++)
                pw.Write(switches[i]);
            pw.Write(enteries.Length);
            for (int i = 0; i < enteries.Length; i++)
            {
                GumpTextEntry entry = enteries[i];
                pw.Write(entry.EntryID);
                pw.Write((ushort)entry.Text.Length * 2);
                pw.WriteUnicodeString(entry.Text);
            }

            return pw.GetBytes();
        }


        public static byte[] WalkRequest(Direction dir)
        {
            PacketWriter pw = new PacketWriter(0x02);
            pw.Write((byte)dir);
            pw.Write(WalkHandling.NextSequence);
            pw.Write((uint)0);
            return pw.GetBytes();
        }
    }

    public enum Direction : byte
    {
        North,
        Northeast,
        East,
        Southeast,
        South,
        Southwest,
        West,
        Northwest
    }
}
