﻿using Phoenix.NewPhoenix;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix
{
    public static partial class Obscuro
    {
        [Command]
        public static Boolean Buff(Int32 id)
        {
            return Phoenix.NewPhoenix.BuffSystem.GetBuff(id);
        }

        [Command]
        public static Boolean Buff(String name)
        {
            return Phoenix.NewPhoenix.BuffSystem.GetBuff(name);
        }

        [Command]
        public static void GumpSendIncoming(UInt32 buttonID, InfoGump infoGump)
        {
            Phoenix.NewPhoenix.Gumps.GumpSendIncoming(buttonID, infoGump);
        }

        [Command]
        public static void GumpSendIncoming(UInt32 buttonID, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches, InfoGump infoGump)
        {
            Phoenix.NewPhoenix.Gumps.GumpSendIncoming(buttonID, gumpTextEntry, gumpSwitches, infoGump);
        }

        [Command]
        public static void GumpSendIncoming(UInt32 buttonID, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches, InfoGump infoGump, Boolean autoOpeningGump)
        {
            Phoenix.NewPhoenix.Gumps.GumpSendIncoming(buttonID, gumpTextEntry, gumpSwitches, infoGump, autoOpeningGump);
        }

        [Command]
        public static void GumpSend(UInt32 itemID, UInt32 buttonID)
        {
            Phoenix.NewPhoenix.Gumps.GumpSend(itemID, buttonID);
        }

        [Command]
        public static void GumpSend(UInt32 itemID, UInt32 button, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches)
        {
            Phoenix.NewPhoenix.Gumps.GumpSend(itemID, button, gumpTextEntry, gumpSwitches);
        }

        [Command]
        public static void GumpSend(UInt32 itemID, UInt32 button, GumpTextEntry[] gumpTextEntry, GumpSwitches[] gumpSwitches, Boolean autoOpeningGump)
        {
            Phoenix.NewPhoenix.Gumps.GumpSend(itemID, button, gumpTextEntry, gumpSwitches, autoOpeningGump);
        }

        [Command]
        public static void GumpInfo(Boolean detailed)
        {
            Phoenix.NewPhoenix.Gumps.ShowGumpInfo(detailed);
        }

        public static NewPhoenix.InfoGump getInfoGump()
        {
            return NewPhoenix.Gumps.infoGump;
        }

        [Command]
        public static void GumpSendedInfo()
        {
            Phoenix.NewPhoenix.Gumps.ShowSendedGump();
        }

        public static NewPhoenix.InfoGumpSended getSendedInfoGump()
        {
            return NewPhoenix.Gumps.infoSendedGump;
        }

        [Command]
        public static void Buy(UInt16[] buyList)
        {
            Phoenix.NewPhoenix.BuyAgent.Buy(buyList);
        }

        [Command]
        public static void Buy(UInt16[] buyList, UInt32[] amountList)
        {
            Phoenix.NewPhoenix.BuyAgent.Buy(buyList, amountList);
        }

        [Command]
        public static void Buy(UInt16[] buyList, UInt32[] priceList, Boolean exactValue)
        {
            Phoenix.NewPhoenix.BuyAgent.Buy(buyList, priceList, exactValue);
        }

        [Command]
        public static void Sell(UInt16[] sellList)
        {
            Phoenix.NewPhoenix.SellAgent.Sell(sellList);
        }

        [Command]
        public static Boolean isBuyLimit()
        {
            return Phoenix.NewPhoenix.BuyAgent.isBuyLimit();
        }

        [Command]
        public static void MoveItemPause(Serial sID, UInt16 amount, UInt32 container, Int32 pause)
        {
            Core.SendToServer(Phoenix.Communication.PacketBuilder.ItemPickupRequest(sID, amount));
            System.Threading.Thread.Sleep(pause);
            Core.SendToServer(Phoenix.Communication.PacketBuilder.ItemDropRequest(sID, 0xFFFF, 0xFFFF, 0, container));
        }

        public static event GumpEventHandler IncomingGump
        {
            add { Gumps.gumpsCallbacks.AddHandler(value); }
            remove { Gumps.gumpsCallbacks.RemoveHandler(value); }
        }
    }
}
