﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.NewPhoenix
{
    public class GumpEventArgs : EventArgs
    {
        private InfoGump infoGump;

        public GumpEventArgs(InfoGump infoGump)
        {
            this.infoGump = infoGump;
        }

        public InfoGump Info
        {
            get { return infoGump; }
        }
    }

    public delegate void GumpEventHandler(object sender, GumpEventArgs e);

    internal class GumpPublicEvent : PublicEvent<GumpEventHandler, GumpEventArgs>
    { 
    }
}
