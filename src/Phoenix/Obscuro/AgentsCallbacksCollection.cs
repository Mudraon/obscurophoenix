﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace Phoenix.NewPhoenix
{
    internal sealed class AgentsCallbacksCollection
    {
        class Event
        {
            public AgentsEventArgs EventArgs;
            public AgentsPublicEvent Handler;
        }

        private readonly object syncRoot = new object();
        private Dictionary<uint, AgentsPublicEvent> list = new Dictionary<uint, AgentsPublicEvent>();

        private Thread workerThread;
        private Queue<Event> eventQueue = new Queue<Event>();
        private AutoResetEvent itemQueuedEvent = new AutoResetEvent(false);

        public AgentsCallbacksCollection()
        {
            workerThread = new Thread(new ThreadStart(AsyncInvokeWorker));
            workerThread.IsBackground = true;
            workerThread.Start();
        }

        ~AgentsCallbacksCollection()
        {
            workerThread.Abort();
        }

        public void Add(uint serial, AgentsEventHandler handler)
        {
            lock (syncRoot)
            {
                AgentsPublicEvent callbacks;
                if (!list.TryGetValue(serial, out callbacks))
                {
                    callbacks = new AgentsPublicEvent();
                    list.Add(serial, callbacks);
                }

                callbacks.AddHandler(handler);
            }
        }

        public void Clear()
        {
            lock (syncRoot)
            {
                list.Clear();
            }
        }

        public void Remove(uint serial, AgentsEventHandler handler)
        {
            lock (syncRoot)
            {
                AgentsPublicEvent callbacks;
                if (list.TryGetValue(serial, out callbacks))
                {
                    callbacks.RemoveHandler(handler);

                    if (callbacks.IsEmpty)
                    {
                        list.Remove(serial);
                    }
                }
            }
        }

        public void Invoke(AgentsEventArgs e)
        {
            lock (syncRoot)
            {
                AgentsPublicEvent callbacks;
                if (list.TryGetValue(e.Info.VendorSerial , out callbacks))
                {
                    callbacks.Invoke(null, e);
                }
            }
        }

        public void InvokeAsync(AgentsEventArgs e)
        {
            lock (syncRoot)
            {
                AgentsPublicEvent callbacks;
                if (list.TryGetValue(e.Info.VendorSerial, out callbacks))
                {
                    Event args = new Event();
                    args.Handler = callbacks;
                    args.EventArgs = e;

                    eventQueue.Enqueue(args);
                    itemQueuedEvent.Set();
                }
            }
        }

        private void AsyncInvokeWorker()
        {
            while (true)
            {
                itemQueuedEvent.WaitOne();

                try
                {
                    while (eventQueue.Count > 0)
                    {
                        Event e = null;
                        lock (syncRoot)
                        {
                            Debug.Assert(eventQueue.Peek() != null, "null in eventQueue.");
                            e = eventQueue.Dequeue();
                        }

                        if (e != null)
                        {
                            e.Handler.Invoke(null, e.EventArgs);
                        }
                    }
                }
                catch { }
            }
        }
    }
}
