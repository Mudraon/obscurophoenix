﻿using Phoenix.Communication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.NewPhoenix
{
    public static class BuffSystem
    {

        private static List<BuffItem> tmp_buff_debuffList;
        private static List<BuffItem> buff_debuffList;

        internal static void Init()
        {
            Core.RegisterServerMessageCallback(0xDF, new MessageCallback(BuffDebuff));
            tmp_buff_debuffList = new List<BuffItem>();
            buff_debuffList = new List<BuffItem>();
            buff_debuffList = buffItemsAdd();
        }

        private static CallbackResult BuffDebuff(byte[] data, CallbackResult prevState)
        {
            if (data[0] != 0xDF) throw new NotImplementedException();

            if (ByteConverter.BigEndian.ToUInt16(data, 1) == 0x0F) // Remove icon
            {
                PacketReader pr = new PacketReader(data);
                byte packet = pr.ReadByte();
                ushort packetSize = pr.ReadUInt16();
                uint playerSerial = pr.ReadUInt32();
                ushort iconNum2Show = pr.ReadUInt16();
                ushort show_remove = pr.ReadUInt16();
                uint unk = pr.ReadUInt32();

                try 
                {
                    BuffItem bsi = FindBuffInformation(iconNum2Show);
                    if (bsi != null)
                    {
                        //UO.Print("Removing {0} from tmp", FindBuffInformation(iconNum2Show).Text);
                        tmp_buff_debuffList.Remove(bsi);
                    }
                }
                catch { /* Something fucked */  }

            }

            if (ByteConverter.BigEndian.ToUInt16(data, 1) != 0x0F) // new buff icon with specificates
            {
                PacketReader pr = new PacketReader(data);
                byte packet = pr.ReadByte();
                ushort packetSize = pr.ReadUInt16();
                uint playerSerial = pr.ReadUInt32();
                ushort iconNum2Show = pr.ReadUInt16();
                ushort show_remove = pr.ReadUInt16();
                uint unk = pr.ReadUInt32();
                ushort iconNum2Show_2 = pr.ReadUInt16();
                ushort show = pr.ReadUInt16();
                uint unk2 = pr.ReadUInt32();
                ushort timeInSec = pr.ReadUInt16();
                ushort unk3 = pr.ReadUInt16();
                byte unk4 = pr.ReadByte();
                uint cliloc1 = pr.ReadUInt32();
                uint cliloc2 = pr.ReadUInt32();
                uint unk5 = pr.ReadUInt32();
                ushort unk6 = pr.ReadUInt16();
                string text = pr.ReadUnicodeString((packetSize - 44));
                ushort unk7 = pr.ReadUInt16();

                BuffItem bsi = GetBuffInformation(iconNum2Show);
                if (!tmp_buff_debuffList.Contains(FindBuffInformation(iconNum2Show)))
                {
                    tmp_buff_debuffList.Add(new BuffItem(bsi.ID, bsi.Text, bsi.Cliloc1, bsi.Cliloc2));
                    //UO.Print("Adding {0} to tmp", FindBuffInformation(iconNum2Show).Text);
                }

            }
            return CallbackResult.Normal;
        }

        private static BuffItem FindBuffInformation(Int32 id)
        {
            return tmp_buff_debuffList.Find(x => x.ID == id);
        }

        private static BuffItem GetBuffInformation(Int32 id)
        {
            return buff_debuffList.Find(x => x.ID == id);
        }

        public static Boolean GetBuff(String buffName)
        {
            BuffItem bsi = tmp_buff_debuffList.Find(x => x.Text == buffName);
            if (bsi != null)
                return true;
            else
                return false;
        }

        public static Boolean GetBuff(Int32 id)
        {
            BuffItem bsi = tmp_buff_debuffList.Find(x => x.ID == id);
            if (bsi != null)
                return true;
            else
                return false;
        }

        private static List<BuffItem> buffItemsAdd()
        {
            List<BuffItem> buff = new List<BuffItem>();
            buff.Add(new BuffItem(1001, "Dismount", 1075635, 1075636));
            buff.Add(new BuffItem(1002, "Disarm", 1075637, 1075638));
            buff.Add(new BuffItem(1005, "Nightsight", 1075643, 1075644));
            buff.Add(new BuffItem(1006, "Death Strike", 0, 0));
            buff.Add(new BuffItem(1007, "Evil Omen", 0, 0));
            buff.Add(new BuffItem(1008, "unknown", 0, 0));
            buff.Add(new BuffItem(1009, "Regeneration", 1044106, 1075106));
            buff.Add(new BuffItem(1010, "Divine Fury", 0, 0));
            buff.Add(new BuffItem(1011, "Enemy Of One", 0, 0));
            buff.Add(new BuffItem(1012, "Stealth", 1044107, 1075655));
            buff.Add(new BuffItem(1013, "Active Meditation", 1044106, 1075106));
            buff.Add(new BuffItem(1014, "Blood Oath caster", 0, 0));
            buff.Add(new BuffItem(1015, "Blood Oaath curse", 0, 0));
            buff.Add(new BuffItem(1016, "Corpse Skin", 0, 0));
            buff.Add(new BuffItem(1017, "Mindrot", 0, 0));
            buff.Add(new BuffItem(1018, "Pain Spike", 0, 0));
            buff.Add(new BuffItem(1019, "Strangle", 0, 0));
            buff.Add(new BuffItem(1020, "Gift of Renewal", 0, 0));
            buff.Add(new BuffItem(1021, "Attune Weapon", 0, 0));
            buff.Add(new BuffItem(1022, "Thunderstorm", 0, 0));
            buff.Add(new BuffItem(1023, "Essence of Wind", 0, 0));
            buff.Add(new BuffItem(1024, "Ethereal Voyage", 0, 0));
            buff.Add(new BuffItem(1025, "Gift Of Life", 0, 0));
            buff.Add(new BuffItem(1026, "Arcane Empowerment", 0, 0));
            buff.Add(new BuffItem(1027, "Mortal Strike", 0, 0));
            buff.Add(new BuffItem(1028, "Reactive Armor", 1075812, 1075813));
            buff.Add(new BuffItem(1029, "Protection", 1075814, 1075815));
            buff.Add(new BuffItem(1030, "Arch Protection", 1075816, 1075816));
            buff.Add(new BuffItem(1031, "Magic Reflection", 1075817, 1075818));
            buff.Add(new BuffItem(1032, "Incognito", 1075819, 1075820));
            buff.Add(new BuffItem(1033, "Disguised", 0, 0));
            buff.Add(new BuffItem(1034, "Animal Form", 0, 0));
            buff.Add(new BuffItem(1035, "Polymorph", 1075824, 1075820));
            buff.Add(new BuffItem(1036, "Invisibility", 1075825, 1075826));
            buff.Add(new BuffItem(1037, "Paralyze", 1075827, 1075828));
            buff.Add(new BuffItem(1038, "Poison", 0x0F8627, 0x1069B1));
            buff.Add(new BuffItem(1039, "Bleed", 0x106a75, 0x106a76));
            buff.Add(new BuffItem(1040, "Clumsy", 0x106a77, 0x106a78));
            buff.Add(new BuffItem(1041, "Feeble Mind", 0x106a79, 0x106a7a));
            buff.Add(new BuffItem(1042, "Weaken", 1075837, 1075838));
            buff.Add(new BuffItem(1043, "Curse", 1075835, 1075836));
            buff.Add(new BuffItem(1044, "Mass Curse", 0x106a7f, 0x106a80));
            buff.Add(new BuffItem(1045, "Agility", 0x106a81, 0x106a82));
            buff.Add(new BuffItem(1046, "Cunning", 0x106a83, 0x106a84));
            buff.Add(new BuffItem(1047, "Strength", 0x106a85, 0x106a86));
            buff.Add(new BuffItem(1048, "Bless", 0x106a87, 0x106a88));
            return buff;
        }
    }

    public class BuffItem
    {
        public Int32 ID { get; set; }
        public String Text { get; set; }
        public UInt32 Cliloc1 { get; set; }
        public UInt32 Cliloc2 { get; set; }

        public BuffItem(int id, string text, uint cliloc1, uint cliloc2)
        {
            this.ID = id;
            this.Text = text;
            this.Cliloc1 = cliloc1;
            this.Cliloc2 = cliloc2;
        }
    }
}
