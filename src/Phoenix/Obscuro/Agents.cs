﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Phoenix.NewPhoenix
{
    public class AgentsInfo
    {
        public uint VendorSerial { get; set; }
        public bool Status { get; set; }
    }
}
